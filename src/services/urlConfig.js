import axios from "axios";
import { userLocal } from "./local.service";

export const BASE_URL = "https://fiverrnew.cybersoft.edu.vn";
export const TOKEN_CYBER =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAzM0UiLCJIZXRIYW5TdHJpbmciOiIyNi8wNC8yMDIzIiwiSGV0SGFuVGltZSI6IjE2ODI0NjcyMDAwMDAiLCJuYmYiOjE2NTQzNjIwMDAsImV4cCI6MTY4MjYxNDgwMH0.GGqFf8-ZXIqAjnaJZ40LjQvUHb1VyvRv3XtEIsMe_qE";
export const configHeader = () => {
  return {
    tokenCybersoft: TOKEN_CYBER,
    token: userLocal.get()?.token,
  };
};
export const https = axios.create({
  baseURL: BASE_URL,
  headers: configHeader(),
});
// Add a request interceptor
